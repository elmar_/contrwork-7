import React, {useState} from "react";
import './App.css';
import Button from "../Components/Button/Button";
import Order from "../Components/Order/Order";

const App = () => {

  const [foods, setfoods] = useState([
  ]);

  const deleteOrder = id => {
    const index = foods.findIndex(food => food.id === id);
    const foodsCopy = [...foods];
    foodsCopy.splice(index, 1);

    setfoods(foodsCopy);
  }

  let total = 0;
  for (let i = 0; i < foods.length; i++) {
    total += foods[i].price;
  }

  const OrderBlock = () => {
    let statusOrder = 'Order is empty';
    if (foods.length > 0) {
      statusOrder = 'Orders';
    }
    return (
      <div className="OrderBlock">
        {statusOrder}
        {foods.map(food => {
          return <Order name={food.name} number={food.number} price={food.price} currency={food.currency} deleteOrder={() => deleteOrder(food.id)}/>
        })}
        <p className="total">Total: {total}</p>
      </div>
    );
  };

  const clickOnButton = info => {
    const foodsCopy = [...foods];
    let fl = 0;
    foodsCopy.map(food => {
      if (food.name === info.name) {
        food.number ++;
        food.price += info.price;
        return fl++;
      }
      return fl;
    });

    if (fl > 0) {
      setfoods(foodsCopy);
    } else {
      foodsCopy.push(info);
      setfoods(foodsCopy);
    }
  };

  const infoForButton = [
    {name: 'Hamburger', price: 80, currency: 'KGS', number: 1, id: 1},
    {name: 'Cheeseburger', price: 90, currency: 'KGS', number: 1, id: 2},
    {name: 'Fries', price: 45, currency: 'KGS', number: 1, id: 3},
    {name: 'Coffee', price: 70, currency: 'KGS', number: 1, id: 4},
    {name: 'Tea', price: 50, currency: 'KGS', number: 1, id: 5},
    {name: 'Cola', price: 40, currency: 'KGS', number: 1, id: 6}
  ];

  const FoodsBox = () => {
    return (
        <div className="FoodBox">
            {infoForButton.map(food => {
          return (<Button
              key={food.id}
              foodsName={food.name}
              pricefoods={food.price}
              currency={food.currency}
              btnClick={() => clickOnButton(food)}
              />);
          })}
        </div>);
  };

  return (
    <div className="App">
      <OrderBlock deleteOrder={deleteOrder}/>
        <FoodsBox clickOnButton={info => clickOnButton(info)}/>
    </div>
  );
}

export default App;
