import React from 'react';
import "./Button.css";

const Button = props => {
    return(
        <div className="Button" onClick={props.btnClick}>
            <h3 className="button-title">{props.foodsName}</h3>
            <p className="button-price">Price: {props.pricefoods} {props.currency}</p>
        </div>
    );
};

export default Button;