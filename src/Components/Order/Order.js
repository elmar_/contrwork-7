import React from 'react';
import './Order.css';

const Order = props => {
    return (
        <div className="Order">
            <span>{props.name}</span>
            <span>x {props.number} {props.price} {props.currency} <button type="button" className="Order-btn" onClick={props.deleteOrder}>Delete</button></span>
        </div>
    );
};

export default Order;